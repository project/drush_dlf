<?php

/**
 * Implements hook_drush_command().
 */
function drush_dlf_drush_command() {

  $items['drush-download-file'] = array(
    'description' => 'Demonstrate how Drush commands work.',
    'aliases' => array('dlf'),
    'arguments' => array(
      'type' => 'The type of statement (error or success).',
    ),
    'options' => array(
      'repeat' => 'The number of statement repeats.',
    ),
    'examples' => array(
      'drush dlf error' => 'Prints the statement once with the error flag.',
      'drush dlf success --repeat=10' => 'Prints the statement 10 times with the success flag.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

/**
 * Callback for the drush-demo-command command
 */
function drush_drush_dlf_drush_download_file($type = FALSE) {

  drush_download_file($url, $destination = FALSE, $cache_duration = 0);

}
